If you make use of any of the modules which download files to the Ansible controller (such as bigip_qkview or bigip_ucs, you should be aware that the Ansible worker that executes your job runs in a restricted environment.
Please refer to the following link to properly setup Tower for accepting file downloads.
https://clouddocs.f5.com/products/orchestration/ansible/devel/usage/module-usage-with-tower.html 
